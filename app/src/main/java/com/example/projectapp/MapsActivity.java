package com.example.projectapp;

import android.Manifest;
import android.app.NotificationManager;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.FragmentActivity;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, RoutingListener, LocationListener {
    ArrayList<LatLng> sensors = new ArrayList<>();
    private Polygon mPolygon = null;
    Location currLocation, mLocation;
    LocationManager mLocationManager;
    LocationListener mLocationListener;
    private LocationCallback locationCallback;
    FusedLocationProviderClient fusedLocationProviderClient;
    GoogleMap mMap;
    MarkerOptions origin, dest;
    Polyline currentPolyline;
    private DatabaseReference mDatabaseReference;
    private ArrayList<LatLng> mLatLngs = new ArrayList<>();
    private List<LatLng> markerList = new ArrayList<>();
    private ArrayList keys1 = new ArrayList();
    private int markerLocation = 0;
    private int geofenceLocation1 = 0;
    private List<Polyline> polylines;
    private static final int[] COLORS = new int[]{R.color.colorPrimaryDark, R.color.colorPrimary, R.color.yellowDark, R.color.colorAccent,R.color.primary_dark_material_light};

    private FloatingActionButton mClear;
    private static final int REQUEST_CODE = 101;
    private static final int DEFAULT_ZOOM = 17;
    private static final long UPDATE_INTERVAL = 1000;
    private static final long FASTEST_INTERVAL = 2500;
    int[] style = {R.raw.style_night, R.raw.style_standard};
    int currentStyle = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        getLastLocation();
//        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();

        polylines = new ArrayList<>();
        mClear = findViewById(R.id.clear_route);

//        mDatabaseReference = FirebaseDatabase.getInstance().getReference("sensors");

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    // Update UI with location data
                    // ...
                }
            }
        };
    }

    private void getLastLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_BACKGROUND_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE);
            return;
        }

        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    currLocation = location;
                    SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

                    assert mapFragment != null;
                    mapFragment.getMapAsync(MapsActivity.this);
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        LatLng latLng = new LatLng(currLocation.getLatitude(), currLocation.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM));
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        LatLng sensor1 = new LatLng(-1.309465, 36.8122377);
//        LatLng sensor1 = new LatLng(-1.291267, 36.814710);
        sensors.add(sensor1);

        for (int i = 0; i < sensors.size(); i ++){

            mMap.addMarker(new MarkerOptions()
                                .position(sensors.get(i))
                                .icon(BitmapDescriptorFactory.fromBitmap(smallerMarker())));
        }
        getMarkers();
//        displayAddedGeofences();
        LatLng userLocation = new LatLng(currLocation.getLatitude(), currLocation.getLongitude());
//        geofenceListener(userLocation);
    }

    public Bitmap smallerMarker(){
        int height = 100;
        int width = 100;
        BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.mipmap.ic_parking_foreground);
        Bitmap b = bitmapdraw.getBitmap();
        return Bitmap.createScaledBitmap(b, width, height, false);
    }

    public void getMarkers (){
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference ref = firebaseDatabase.getReference("sensors");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    final MapModel map = new MapModel();
                    keys1.add(postSnapshot.getKey());

                    ref.child(String.valueOf(keys1.get(geofenceLocation1))).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot1) {
                            Log.d("MapActivity", String.valueOf(dataSnapshot1.child("Latitude").getValue()));
                            Log.d("MapActivity", String.valueOf(dataSnapshot1.child("Longitude").getValue()));
                            Log.d("MapActivity", String.valueOf(dataSnapshot1.child("Status").getValue()));

                            map.setLatitude(String.valueOf(dataSnapshot1.child("Latitude").getValue()));
                            map.setLongitude(String.valueOf(dataSnapshot1.child("Longitude").getValue()));
                            map.setStatus(Boolean.parseBoolean(String.valueOf(dataSnapshot1.child("Status").getValue())));

                            if (map.isStatus()){
                                LatLng latLng = new LatLng(Double.parseDouble(map.getLatitude()), Double.parseDouble(map.getLongitude()));
                                int height = 100;
                                int width = 100;
                                BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.mipmap.ic_parking_foreground);
                                Bitmap b = bitmapdraw.getBitmap();
                                Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
                                mMap.addMarker(new MarkerOptions()
                                        .position(latLng)
                                        .icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
                                sensors.add(latLng);
                            }
                            geofenceLocation1 ++;
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }

                    });
                }
                geofenceLocation1 = 0;

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Failed to read value
                Log.e("MapActivity", "Failed to read value.", databaseError.toException());
            }
        });
        Log.d("MapActivity", sensors.toString());
    }

    public void setNotification(String s){
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_logo_icon_orange)
                .setContentTitle("SMPark")
                .setContentText(s)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(s))
                .setPriority(NotificationCompat.PRIORITY_MAX);
        NotificationManager notificationManager = ( NotificationManager ) getSystemService( NOTIFICATION_SERVICE );
        notificationManager.notify(1, mBuilder.build());
//
//        // When you issue multiple notifications about the same type of event,
//        // it’s best practice for your app to try to update an existing notification
//        // with this new information, rather than immediately creating a new notification.
//        // If you want to update this notification at a later date, you need to assign it an ID.
//        // You can then use this ID whenever you issue a subsequent notification.
//        // If the previous notification is still visible, the system will update this existing notification,
//        // rather than create a new one. In this example, the notification’s ID is 001//
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation();
            }
        }
    }

    public void myLocation(View view) {
        LatLng latLng = new LatLng(currLocation.getLatitude(), currLocation.getLongitude());
        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
    }

    public void toggleStyle(View view) {
        if (currentStyle == style.length && style.length != 0) {
            currentStyle = 0;
        }

        try {
            mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, style[currentStyle++]));

        } catch (Resources.NotFoundException e) {
            Log.e("MapsActivity", "Cannot find style.", e);
        }
    }

    public void calcDistance (View view){
        if (sensors.size() == 0){
            Toast.makeText(this, "There are no available parking spots", Toast.LENGTH_LONG).show();
        } else {
            HashMap<Integer, Double> distance = new HashMap<>();

            Location location1 = new Location("");
            location1.setLatitude(currLocation.getLatitude());
            location1.setLongitude(currLocation.getLongitude());

            for (int i = 0; i < sensors.size(); i ++){
                Location location2 = new Location("");
                location2.setLatitude(sensors.get(i).latitude);
                location2.setLongitude(sensors.get(i).longitude);

                double dis = location1.distanceTo(location2);
                Log.d("DISTANCE", String.valueOf(dis));

                distance.put(i, dis);
            }
            double minDis = Collections.min(distance.values());
            int key = 0;

            if (minDis > 1000){
                Toast.makeText(this, "There are no parking spots close to your location", Toast.LENGTH_LONG).show();
            } else{
                for (Map.Entry<Integer, Double> entry : distance.entrySet()) {
                    if (entry.getValue().equals(minDis)) {
                        key = entry.getKey();
                    }
                }
                Log.d("DISTANCE", sensors.get(key) + " has the shortest distance");
                LatLng latLng = new LatLng(currLocation.getLatitude(), currLocation.getLongitude());
                Log.d("DISTANCE", "From " + latLng + " to " + sensors.get(key));
                getRoute(sensors.get(key));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(17));

                mClear.show();
            }
        }

    }

    private void getRoute(LatLng des) {
        LatLng latLng = new LatLng(currLocation.getLatitude(), currLocation.getLongitude());

        Routing routing = new Routing.Builder()
                .key("AIzaSyCBHdkX44gzYFr0_sbepYJK5rFVxS11GeY")
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .withListener(this)
                .alternativeRoutes(true)
                .waypoints(latLng, des)
                .build();
        routing.execute();
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        // The Routing request failed
        if(e != null) {
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
            Log.e("ERROR", "Error: " + e.getMessage());
        }else {
            Toast.makeText(this, "Something went wrong, Try again", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {
        if(polylines.size()>0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
        }

        polylines = new ArrayList<>();
        //add route(s) to the map.
        for (int i = 0; i <route.size(); i++) {

            //In case of more than 5 alternative routes
            int colorIndex = i % COLORS.length;

            PolylineOptions polyOptions = new PolylineOptions();
//            polyOptions.color(getResources().getColor(COLORS[colorIndex]));
            polyOptions.color(Color.rgb(0, 132, 248));
            polyOptions.width(15);
            polyOptions.addAll(route.get(i).getPoints());
            Polyline polyline = mMap.addPolyline(polyOptions);
            polylines.add(polyline);
        }
    }

    @Override
    public void onRoutingCancelled() {

    }

    public void erasePolyline(View view) {
        for (Polyline polyline : polylines){
            polyline.remove();
        }
        polylines.clear();

        mClear.hide();

    }

    @Override
    public void onLocationChanged(Location location) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
            return;
        }

        LatLng myLocation = new LatLng(location.getLatitude(), location.getLongitude());

        Log.d("ONLOCATION", "Location has changed to: " + myLocation.toString());
        Toast.makeText(MapsActivity.this, "Location has changed to: " + myLocation.toString(), Toast.LENGTH_LONG).show();
        setNotification("Location has changed");
    }
}
