package com.example.projectapp.Models;

import java.util.List;

public class LegsObject {

    private Distance distance;
    private Duration duration;
    private List<StepsObject> steps;
    public LegsObject(Distance distance, Duration duration, List<StepsObject> steps) {
        this.distance = distance;
        this.duration = duration;
        this.steps = steps;
    }
    public List<StepsObject> getSteps() {
        return steps;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    public class Distance {
        private String text;
        private int value;

        private Distance(String text, int value) {
            this.text = text;
            this.value = value;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }

    public class Duration {
        private String text;
        private int value;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        private Duration(String text, int value) {
            this.text = text;
            this.value = value;
        }
    }
}
